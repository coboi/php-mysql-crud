<!DOCTYPE html>
<html>
<head>
	<title>Create Read Update Delete dengan PHP dan MySQL</title>
</head>
<body>
	<form action="tambah-aksi.php" method="POST">
		<table border="0" width="40%">
			<tr>
				<td colspan="2"><b>Tambah Barang</b></td>
			</tr>
			<tr>
				<td>Kode Barang</td>
				<td><input type="text" name="kode_barang" required></td>
			</tr>
			<tr>
				<td>Nama Barang</td>
				<td><input type="text" name="nama_barang" required></td>
			</tr>
			<tr>
				<td>Satuan</td>
				<td><input type="text" name="satuan" required></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="kirim" value="Tambah"> <input type="reset" name="batal" value="Batal"> <button><a href="index.php">Kembali</a></button></td>
			</tr>
		</table>
	</form>
</body>
</html>