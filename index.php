<!DOCTYPE html>
<html>
<head>
	<title>Create Read Update Delete dengan PHP dan MySQL</title>
</head>
<body>
	<h1>Create Read Update Delete dengan PHP dan MySQL</h1>
	<table border="1" width="40%">
		<tr>
			<td colspan="4"><a href="tambah.php">Tambah Barang</a></td>
		</tr>
		<tr>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Satuan</th>
			<th>Aksi</th>
		</tr>
		<?php
			include "koneksi.php";

			$query = mysql_query("select * from tbl_barang");
			while ($data = mysql_fetch_array($query)) {
		?>
		<tr>
			<td><?php echo $data['kode_barang']; ?></td>
			<td><?php echo $data['nama_barang']; ?></td>
			<td><?php echo $data['satuan']; ?></td>
			<td><a href="edit.php?kode_barang=<?php echo $data['kode_barang'] ?>">Edit</a> | <a href="hapus-aksi.php?kode_barang=<?php echo $data['kode_barang'] ?>">Hapus</a></td>
		</tr>
		<?php
			}
		?>
	</table>
</body>
</html>